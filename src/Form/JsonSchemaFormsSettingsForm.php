<?php

namespace Drupal\json_schema_forms\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form for the json_schema_forms module.
 */
class JsonSchemaFormsSettingsForm extends ConfigFormBase {

  /**
   * Configuration storage name.
   *
   * @var string
   */
  private const CONFIG_NAME = 'json_schema_forms.settings';

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return [
      static::CONFIG_NAME,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'json_schema_forms_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config(static::CONFIG_NAME);

    $form['schema_storage'] = [
      '#type' => 'select',
      '#title' => $this->t('JSON Schema Storage'),
      '#options' => [
        'file' => $this->t('Filesystem'),
        'db' => $this->t('Database'),
      ],
      '#default_value' => $config->get('schema_storage'),
      '#required' => TRUE,
    ];

    $form['schema_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Schema Path'),
      '#default_value' => $config->get('schema_path'),
      '#states' => [
        'visible' => [
          'select[name="schema_storage"]' => [
            'value' => 'file',
          ],
        ],
        'required' => [
          'select[name="schema_storage"]' => [
            'value' => 'file',
          ],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(static::CONFIG_NAME);
    $config->set('schema_storage', $form_state->getValue('schema_storage'));
    $config->set('schema_path', $form_state->getValue('schema_path'));
    $config->save();
    return parent::submitForm($form, $form_state);
  }

}
